import React from 'react'
import './Footer.css'
import {Icon} from "semantic-ui-react";

const Footer = () => {

    return (
        <div className='footer'>
             <span> <Icon name='copyright' /> Copyright {(new Date()).getFullYear()}</span>
        </div>
    )
}

export default Footer