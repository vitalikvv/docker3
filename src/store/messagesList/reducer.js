import {messagesListActionTypes} from "./actions";

const initialState ={
    messages: [],
    currentUser: {},
    isOpenModal: false,
    startLoading: false,
    likeOrDisValue: '',
    isOpenToast: false
}

export const chat = (state = initialState, action) => {
    switch (action.type) {
        case messagesListActionTypes.SET_MESSAGES:
            return {
                ...state,
                messages: action.payload
            }
        case messagesListActionTypes.SET_CURRENT_USER:
            return {
                ...state,
                currentUser: action.payload
            }
        case messagesListActionTypes.SET_OPEN_MODAL:
            return {
                ...state,
                isOpenModal: action.payload
            }
        case messagesListActionTypes.ADD_MESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.payload]
            }
        case messagesListActionTypes.EDIT_MESSAGE:
            const updatedMessages = state.messages.map(message => {
                if (message.id === action.payload.messageId) {
                    const newMessage = {
                        ...message,
                        text: action.payload.newMessageText
                    }
                    return newMessage;
                }
                return message;
            })
            return {
                ...state,
                messages: updatedMessages
            }
        case messagesListActionTypes.LIKE_MESSAGE:
            const {messageId, likeOrDis} = action.payload;
            const newMessages = state.messages.map(message => {
                if (message.id === messageId) {
                    likeOrDis === 'like' ? localStorage.setItem(messageId,messageId) : localStorage.removeItem(messageId)
                    const newMessage = {
                        ...message,
                        likesCount: likeOrDis === 'like' ? message.likesCount + 1 : message.likesCount - 1
                    }
                    return newMessage;
                }
                return message;
            })
            return {
                ...state,
                messages: newMessages
            }
        case messagesListActionTypes.DELETE_MESSAGE:
            return {
                ...state,
                messages: state.messages.filter(message => message.id !== action.payload)
            }
        case messagesListActionTypes.SET_START_LOADING:
            return {
                ...state,
                startLoading: action.payload
            }
        case messagesListActionTypes.SET_LIKE_OR_DIS:
            return {
                ...state,
                likeOrDisValue: action.payload
            }
        case messagesListActionTypes.SET_OPEN_TOAST:
            return {
                ...state,
                isOpenToast: action.payload
            }
        default:
                return state
    }
}