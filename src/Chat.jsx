import React, {useEffect} from 'react'
import MainHeader from './components/Header/Header';
import Preloader from "./components/Preloader/Preloader";
import MessageInput from "./components/MessageInput/MessageInput";
import MessageList from "./components/MessageList/MessageList";
import Footer from './components/Footer/Footer';
import {callApi} from "./helpers/apiHelper";
import {changeDateFormat} from "./helpers/changeDateFormat";
import {getCurrentUser} from "./helpers/getCurrentUser";
import Toast from 'react-toast-component';
import {connect} from "react-redux";
import {
    addMessage,
    deleteMessage,
    setCurrentUser,
    setMessages,
    setOpenToast,
    setStartLoading
} from "./store/messagesList";

const Chat = (props) => {
    useEffect( () => {
        props.setStartLoading(true);
        getData ()
        localStorage.clear();
    }, [])

    async function getData () {
        const response = await callApi('https://edikdolynskyi.github.io/react_sources/messages.json');
        const dataWithChangedDateFormat = changeDateFormat(response);
        props.setMessages(dataWithChangedDateFormat)
        const currentUser = await getCurrentUser();
        props.setCurrentUser(currentUser)
        props.setStartLoading(false);
    }

    return (
        <div className='chat'>
            { props.startLoading ? <Preloader/> : null}
            <MainHeader messages={props.messages}/>
            <MessageList />
            <MessageInput
                addNewMessage={props.addMessage}
                currentUser={props.currentUser}
            />
            <Footer/>
            <Toast
                isOpen={props.isOpenToast}
                hasAutoDismiss={true}
                closeCallback={() => props.setOpenToast(false)}
                description={props.likeOrDisValue === 'like' ? 'You liked post.' : 'You disliked post.'}
                title="Notification!"
                duration={3000}
                classNames={props.likeOrDisValue === 'like' ? ['success', 'notification-message-like'] : ['error', 'notification-message-dislike']}
            />
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        currentUser: state.chat.currentUser,
        startLoading: state.chat.startLoading,
        likeOrDisValue: state.chat.likeOrDisValue,
        isOpenToast: state.chat.isOpenToast
    }
}

const ChatWithState = connect( mapStateToProps,
    {setMessages, setCurrentUser, setStartLoading, addMessage, deleteMessage, setOpenToast} )(Chat)

export default ChatWithState